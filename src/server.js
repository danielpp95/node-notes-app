const express = require('express')
const path = require('path');
const exphbs = require('express-handlebars');
const methodOverride = require('method-override');
const session = require('express-session');
const flash = require('connect-flash');
const passport = require('passport')

// === === === === === === === === ===
// |||    INITIALIZATION            |||
// === === === === === === === === ===
// #region === === === === === === ===
  const app = express();
  require('./config/database');
  require('./config/passport');
// #endregion


// === === === === === === === === ===
// |||    SETTINGS                  |||
// #region === === === === === === ===
  app.set('port', process.env.PORT || 3000);
  app.set('views', path.join(__dirname, 'views'));
  app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs'
  }));
  app.set('view engine', '.hbs');
// #endregion

// === === === === === === === === ===
// |||    MIDDLEWARES               |||
// === === === === === === === === ===
// #region === === === === === === ===
  app.use(express.urlencoded({extended: false}));
  app.use(methodOverride('_method'));
  app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
  }));
  app.use(passport.initialize())
  app.use(passport.session())
  app.use(flash());
// #endregion

// === === === === === === === === ===
// |||    GLOBAL VARIABLES          |||
// === === === === === === === === ===
// #region
  app.use((req, res, next) => {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
  })
// #endregion

// === === === === === === === === ===
// |||    ROUTES                    |||
// === === === === === === === === ===
// #region
  app.use(require('./routes/index'));
  app.use(require('./routes/notes'));
  app.use(require('./routes/users'));
// #endregion

// === === === === === === === === ===
// |||    STATIC FILES              |||
// === === === === === === === === ===
// #region
  app.use(express.static(path.join(__dirname, 'public')));
// #endregion

// === === === === === === === === ===
// |||     SERVER IS LISTENING       |||
// === === === === === === === === ===
// #region === === === === === === ===
  app.listen(app.get('port'), () => {
    console.log(`Server on port: ${app.get('port')}`);
  })
// #endregion