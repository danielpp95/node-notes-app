const router = require('express').Router();
const User = require('../models/users');
const passport = require('passport')

router.get('/users/signin', (rep, res) => {
  res.render('users/signin')
})

router.post('/users/signin', passport.authenticate('local', {
  successRedirect: '/notes',
  failureRedirect: '/users/signin',
  failureFlash: true
}))

router.get('/users/signup', (rep, res) => {
  res.render('users/signup')
})

router.post('/users/signup', async (req, res) => {
  const { name, email, password, confirm_password} = req.body;
  const errors = Validate(name, email, password, confirm_password);

  const mailExits = await User.findOne({email})
  mailExits && errors.push({text: 'This mail is currently registered'});

  errors.length > 0 && req.flash('error_msg', errors);
  
  (errors.length > 0 )? 
    res.render('users/signup', {errors, name, email})
  : 
    async function ()  {
      const newUser = new User({name, email, password});
      newUser.password = await newUser.encryptPassword(password)
      await newUser.save();

      req.flash('success_msg', "You are registered")

      res.redirect('/users/signin')
    }()
})

router.get('/users/logout', (req, res) => {
  req.logOut();
  res.redirect('/')
})

function Validate (name, email, password, confirm_password) {
  const errors = [];

  !name && errors.push({text: `Name can't be empty`});
  !email && errors.push({text: `Email can't be empty`});
  password != confirm_password && errors.push({text: 'Passwod do not match'});
  password.length < 4 && errors.push({text: 'Passwod must be at least 4 characters'});

  return errors;
}

module.exports = router;