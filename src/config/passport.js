const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/users')

passport.use(new LocalStrategy({
  usernameField: 'email'
},async (email, password, done) => {
  const user = await User.findOne({email});
  
  !user ? 
    done(null, false, {message: 'Not User Found'})
  : 
    async function() {
      const match = await user.matchPassword(password);
      console.log(match)
      match ?
        done(null, user)
      :
        done(null, false, {message: 'Incorrect Password'})
    }()
}))

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    done(err, user);
  });
});